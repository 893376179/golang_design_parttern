package chain

import "testing"

func TestExampleChain(t *testing.T) {
	c1 := NewProjectManagerChain()
	c2 := NewDepManagerChain()
	c3 := NewGeneralManagerChain()

	c1.SetSuccessor(c2)
	c2.SetSuccessor(c3)

	var c Manager = c1
	c.HandleFeeRequest("Bob", 400)
	c.HandleFeeRequest("Tom", 1400)
	c.HandleFeeRequest("Ada", 10000)
	c.HandleFeeRequest("Floar", 400)
}

// Project manager permit Bob 400 fee request
// Dep manager permit Tom 1400 fee request
// General manager permit Ada 10000 fee request
// Project manager don't permit Floar 400 fee request
