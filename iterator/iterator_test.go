package iterator

import "testing"

func TestExampleIterator(t *testing.T) {
	var aggregator Aggregate
	aggregator = NewNumbers(1, 10)

	IteratorPrint(aggregator.Iterator())
}

// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
