package command

import "testing"

func TestExampleCommand(t *testing.T) {
	mb := &MotherBoard{}
	startCommand := NewStartCommand(mb)
	rebbotCommand := NewRebootCommand(mb)

	box1 := NewBox(startCommand, rebbotCommand)
	box1.PressButton1()
	box1.PressButton2()

	box2 := NewBox(rebbotCommand, startCommand)
	box2.PressButton1()
	box2.PressButton2()

}

// system starting
// system rebooting
// system rebooting
// system starting
