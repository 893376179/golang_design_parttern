package visitor

import "testing"

func TestExampleRequestVisitor(t *testing.T) {
	c := &CustomCol{}
	c.Add(NewEnterpriseCustomer("A company"))
	c.Add(NewEnterpriseCustomer("B company"))
	c.Add(NewIndividualCustomer("Bob"))
	c.Accept(&ServiceRequestVisitor{})
}

// serving enterprise customer A company
// serving enterprise customer B company
// serving individual customer Bob

func TestExampleAnalysis(t *testing.T) {
	c := &CustomCol{}
	c.Add(NewEnterpriseCustomer("A company"))
	c.Add(NewEnterpriseCustomer("B company"))
	c.Add(NewIndividualCustomer("Bob"))
	c.Accept(&AnalysisVisitor{})

}

// analysis enterprise customer A company
// analysis enterprise customer B company
