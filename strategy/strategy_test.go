package strategy

import "testing"

func TestExamplePayCash(t *testing.T) {
	payment := NewPayment("Ada", "0001", 100, &Cash{})
	payment.Pay()
}

// Pay $100 to Ada by cash

func TestExamplePayByBank(t *testing.T) {
	payment := NewPayment("Bob", "0002", 200, &Bank{})
	payment.Pay()
}

// Pay $200 to Bob by bank account 0002
