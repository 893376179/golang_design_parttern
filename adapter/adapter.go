// 适配器模式
package adapter

// Target 是适配的目标接口
type Target interface {
	Request() string
}

// Adapter 是被适配的目标接口
type Adaptee interface {
	SpecificRequest() string
}

// NewAdaptee是被适配接口的工厂函数
func NewAdaptee() Adaptee {
	return &adapteeImpl{}
}

//adapteeImpl是被适配的目标类
type adapteeImpl struct{}

// 目标类下的方法
func (e *adapteeImpl) SpecificRequest() string {
	return "adaptee method"
}

// Adapter的工厂函数
func NewAdapter(adaptee Adaptee) Target {
	return &adapter{
		Adaptee: adaptee,
	}
}

type adapter struct {
	Adaptee
}

func (a *adapter) Request() string {
	return a.SpecificRequest()
}
