package memento

import "testing"

func TestExampleGame(t *testing.T) {
	game := &Game{
		hp: 10,
		mp: 10,
	}
	game.Status()
	process := game.Save()

	game.Play(-2, -1)
	game.Status()

	game.Load(process)
	game.Status()
}

// Current HP:10, MP:10
// Current HP:9, MP:8
// Current HP:10, MP:10
