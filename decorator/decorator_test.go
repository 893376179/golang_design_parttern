package decorator

import (
	"fmt"
)

func ExampleDecorator() {
	var c Component = &ConcreteComponent{}
	c = WarpMulAddDecorator(c, 10)
	c = WarpMulAddDecorator(c, 8)
	res := c.Calc()

	fmt.Printf("res = %d\n", res)
	// res = 18
}
